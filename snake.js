
const KEYS = {'left': 37, 'right': 39, 'up': 38, 'down': 40, 'stop': 83};

// inline defintion of a "struct". this is instead of using Object({x:.., y:..}) all the time
// this is just a tuple
let Point = (_x,_y) => ({x:_x, y:_y});
let cpyPoint = (to, from) => (to.x=from.x, to.y=from.y);

let delay_steps = 100;
let stop_engine=true;

let tail_q = []; // use "push" and "shift" Point-s which represents all the tail positions in a queue manner
let player_pos; // keeping track of head position
let tail_size_max = 7;
let tail_size_cur = 0;
let last_delta = Point(0,0);
let cur_delta = Point(1,0);
let player_base_div = "<div class='circle'></div>";

// moving player to given position, tail follows
// positions: [tail][player][*non-existant target*]
// invalid position just make us stop in place until valid command issues
function movePlayer(dx, dy) {
    // check out of bounds before moving
    if(isOOB(player_pos.x+dx, player_pos.y+dy)) return;
    // check hitting self
    if(getAt(player_pos.x+dx, player_pos.y+dy).isPlayer()) return;

    // remove the tail edge
    if(tail_q.length>0 && tail_size_cur == tail_size_max) {
        let edge = tail_q.shift();
        getAt(edge.x, edge.y).clear();
    } else {
        tail_size_cur++;
    }
    // adding a new segment before the head
    tail_q.push(Point(player_pos.x, player_pos.y));
    // moving the head
    player_pos.x += dx;
    player_pos.y += dy;
    
    getAt(player_pos.x, player_pos.y).setContent(player_base_div, true);
}

function movePlayerAuto() {
    // not allowing to move back, only in 90 degrees, simply ignoring that command by issueing a move to the same direction we were at
    if(last_delta.x + cur_delta.x == 0 && last_delta.y + cur_delta.y == 0) {
        cpyPoint(cur_delta,last_delta); // cur <- last
    }
    movePlayer(cur_delta.x, cur_delta.y);
    cpyPoint(last_delta, cur_delta); // last <- cur
}

function setDirection(x, y) {
    //@NOTE this method should be sync and not async, in case we update one without the other and as we get interrupted
    cur_delta = Point(x,y);
}

function initKeys() {
    document.addEventListener('keydown', (e) => {
        switch(e.keyCode) {
            case KEYS['stop']: 
                stop=!stop;
                if(!stop) engine();
                break;
            case KEYS['right']: setDirection(+1, +0); break;
            case KEYS['left']: setDirection(-1, +0); break;
            case KEYS['up']: setDirection(+0, +1); break;
            case KEYS['down']: setDirection(+0, -1); break;
            default:
                stop=true;
        }
    });
		
}

function initPlayer() {
    //getAt(2,2).innerHTML = "<div class='circle'></div>";
    getAt(2,2).setContent(player_base_div, true);
    player_pos = Point(2, 2);
}

function engine() {
    if(stop) return;
    setTimeout(engine, delay_steps);
    movePlayerAuto();
}