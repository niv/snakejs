
// "main" if u will
// alternative to DOMContentLoaded
document.onreadystatechange = function () {
    if (document.readyState == "complete") {
        // default matrix
        resetMatrix(MATRIX_DEFAULT_DIMENSION);
        initKeys();
        initPlayer();
    }
};
