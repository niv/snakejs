class Square {
    constructor(params=null) {
        this.div = document.createElement("div");
        this.div.setAttribute("class", "square");
        this.containsPlayer = false;
    }

    getDiv() { return this.div; }

    isPlayer() { return this.containsPlayer; }

    getContent() {return this.div.innerHTML; }

    setContent(obj, is_player) { 
        this.div.innerHTML = obj; 
        this.containsPlayer = is_player;
    }

    clear() {
        this.setContent("", false);
    }
}