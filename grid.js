
const MATRIX_DEFAULT_DIMENSION = 15;

// array of Square() objects
let grid = [[]];

function initTableNoJQ(matrix, dimX, dimY) {
    // creates <table> 
    table = document.createElement("table");

    grid = [];

    // creating all cells
    for(let j = 0; j < dimY; j++) {
        // creates a <tr> element
        row = document.createElement("tr");

        // adding array of each row to the whole grid thats how we build 2D array. this means the first element in the grid is Y and not X
        row_grid = []

        for(let i = 0; i < dimX; i++) {
            // creates a <td> element
            cell = document.createElement("td");
            // creates inner div
            square = new Square();
            row_grid.push(square);
            // appends into the cell <td>
            cell.appendChild(square.div);
            // appends the cell <td> into the row <tr>
            row.appendChild(cell);
        }
        // appends the row <tr> into <table>
        table.appendChild(row);

        grid.push(row_grid); 
    }

    // appends <table> into <body>
    matrix.appendChild(table);
}

function isOOB(x, y) {
    if(x<0 || y<0 || x>=MATRIX_DEFAULT_DIMENSION || y>=MATRIX_DEFAULT_DIMENSION) return true;
    return false;
}


function resetMatrix(dim) {
    let matrix = document.getElementById('matrix');
    matrix.innerHTML = "";
    initTableNoJQ(matrix, dim, dim);
}

function getAt(x, y) {
    if(isOOB(x,y)) return null;
    return grid[MATRIX_DEFAULT_DIMENSION-y-1][x];
}


